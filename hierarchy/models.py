from django.db import models

from mptt.models import MPTTModel, TreeForeignKey
class Foo(MPTTModel):
    title = models.CharField(max_length=100, unique=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='childrens', verbose_name='parent')

    def __unicode__(self):
        if self.parent:
            return "child " + self.title + " parent " + self.parent.title
        else:
            return "parent " + self.title


from django.contrib.admin import site
site.register(Foo)