from django.db import models

class Place(models.Model):
    address = models.CharField(max_length=255)

    def __unicode__(self):
        return self.address

from django.contrib.admin import site
site.register(Place)